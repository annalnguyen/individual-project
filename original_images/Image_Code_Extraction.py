import xml.etree.ElementTree as _xml
import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets, svm, metrics
import cv2
import csv
import os
import random
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier
from keras.models import Sequential, Model
from keras.layers import Dense, Lambda, LSTM, Flatten, Dropout, Reshape, Activation, GlobalAveragePooling2D
from keras.applications import vgg16
from keras import optimizers
from keras.utils import to_categorical
import tensorflow as tf
from keras.utils import to_categorical
from keras import losses
from keras.layers import Conv2D, MaxPooling2D
from multiprocessing import Pool
from typing import List, Any
import numpy as np
import cv2
from skimage.measure import compare_ssim as ssim
import matplotlib.pyplot as plt


def main():
    counts = range(1, 101)
    things = zip(counts, rates)
    with Pool(2) as p:
        p.starmap(testLR, things)
    # xArray = []
    # yArray = []
    # xAvg = []
    # yAvg = []
    # total = 0
    # count = 0
    #
    # for filename in os.listdir(
    #     r"C:\Users\Anna\Documents\GTC work\AdvAI\Individual Project\individual-project-master\original_images"):
    #     if filename.endswith(".xml"):
    #         tree = _xml.parse(filename)
    #         #extractPositive(xArray, yArray, xAvg, yAvg, total, count, filename, tree)
    #         extractNegative(xArray, yArray, xAvg, yAvg, total, count, filename, tree)
def getLengthFolders():
    lengthNeg = len(os.listdir(r"C:\Users\Anna\Documents\GSU work\images\2019-04-24 14.11.33_22-33 neg"))
    lengthPos = len(os.listdir(r"C:\Users\Anna\Documents\GSU work\images\2019-04-24 14.11.33_22-33 pos"))

def getNegPosLists():
    negList = []
    posList = []
    num = 0
    p_path = r"C:\Users\Anna\Documents\GSU work\images\2019-04-24 14.11.33_22-33 neg"
    for filename in os.listdir(p_path):
        path = os.path.join(p_path, filename)
        img = cv2.imread(path)
        num += 1
        negList.append(img)
    num = 0
    p_path2 = r"C:\Users\Anna\Documents\GSU work\images\2019-04-24 14.11.33_22-33 pos"
    for file in os.listdir(p_path2):
        path = os.path.join(p_path2, file)
        img = cv2.imread(path)
        num += 1
        posList.append(img)
    return negList, posList

def makeLabels(negList, posList):
    labelNeg = np.zeros(len(negList))
    labelPos = np.ones(len(posList))
    # print(len(negList))
    # print(len(posList))
    return labelNeg, labelPos

def reshape(labelPos, labelNeg, negList, posList):
    total = np.concatenate((negList, posList))
    totalLabel = np.concatenate((labelNeg, labelPos))
    totalLabel = to_categorical(totalLabel)
    n_samples = len(total)
    data = np.reshape(total, (n_samples, -1))

    return data, n_samples, total, totalLabel

def randomizeArrays(total, totalLabel):
    random.seed(900)
    random.shuffle(total)
    random.seed(900)
    random.shuffle(totalLabel)
    return total, totalLabel

def testLR(count, learn):
    lossesList = ["mean_squared_error", "mean_absolute_error", "mean_absolute_percentage_error", "squared_hinge", "squared_hinge"]

    adadelta = optimizers.Adadelta(lr=learn, rho=0.95, epsilon=None, decay=0.0)
    sdg = optimizers.SGD(lr=learn, momentum=0.0, decay=0.0, nesterov=False)
    rms = optimizers.RMSprop(lr=learn)
    adam = optimizers.adam(lr=learn)
    adagrad = optimizers.Adagrad(lr=learn, epsilon=None, decay=0.0)
    optimizersList = [adam, adadelta, adagrad, rms, sdg]
    optimizersNames = ["adam", "adadelta", "adagrad", "rms", "sgd"]
    for i in range(len(optimizersList)):

        x_train, x_test, y_train, y_test = train_test_split(total, totalLabel, test_size=.2)
        baseModel = vgg16.VGG16(weights="imagenet", include_top=False, input_shape=(32, 32, 3))
        x = baseModel.output
        x = GlobalAveragePooling2D()(x)
        x = Dense(1024, activation='relu')(x)
        predictions = Dense(2, activation='softmax')(x)
        model = Model(inputs=baseModel.input, outputs=predictions)
        for layer in baseModel.layers:
            layer.trainable = False
        model.compile(optimizer=optimizersList[i], loss=lossesList[i], metrics=["accuracy"])
        history = model.fit(x_train, y_train, batch_size=3, epochs=5, validation_data=(x_test, y_test), shuffle=True)

        with open("{0}{1}{2}".format(optimizersNames[i], "_" + str(count) ,".csv"), "w") as csvfile:
            writer = csv.writer(csvfile, delimiter = ",")
            writer.writerow([history.history['val_acc']])
        csvfile.close()

def getRates():
    ADAMrates = 10**np.random.uniform(-4, -1, size = 100)
    DELTArates = 10**np.random.uniform(-2, -1, size = 100)
    GRADrates = 10**np.random.uniform(-4,-1, size = 100)
    RMSrates = 10**np.random.uniform(-5, -1, size = 100)
    SGDrates = 10**np.random.uniform(-3, -1, size = 100)

    return ADAMrates, DELTArates, GRADrates, RMSrates,SGDrates

def extractPositive(xArray,yArray,xAvg,yAvg,total, count, filename, tree):
        for x in tree.iter("Vertex"):
            xArray.append(x.get("X"))
        for y in tree.iter("Vertex"):
            yArray.append(y.get("Y"))
        for elem in range(len(xArray)):
            if count > 3:
                total = int(total / 4)
                xAvg.append(total)
                count = 0
                total = 0
            total += int(float(xArray[int(elem)]))
            count += 1
        count = 0
        total = 0
        for element in range(len(yArray)):
            if count > 3:
                total = int(total / 4)
                yAvg.append(total)
                count = 0
                total = 0
            total += int(float(yArray[int(element)]))
            count += 1
        name = os.path.splitext(filename)[0]
        nameTemp = name+"{}{}{}"
        img = cv2.imread(name+ ".tif", 3)
        dim1 = 16
        num = 0
        folder = r"C:\Users\Anna\Documents\GTC work\AdvAI\Individual Project\individual-project-master\original_images\positive images\\"
        height, width = img.shape[:2]
        for count in range(len(xAvg)):
            if xAvg[count] - dim1 < 0 or xAvg[count] + dim1 > width or yAvg[count] - dim1 < 0 or yAvg[count] + dim1 > height:
                continue
            else:
                region = img[int(yAvg[count] - dim1):int(yAvg[count] + dim1),int(xAvg[count] - dim1):int(xAvg[count] + dim1), :]
                height2, width2 = region.shape[:2]
                if height2 != 32 or width2 != 32: continue
                cv2.imwrite(os.path.join(folder, nameTemp.format("_", num, '.tif')), region)
                num += 1
                if num > 300:
                    break
        return 0
def extractNegative(xArray,yArray,xAvg,yAvg,total, count, filename, tree):
    for x in tree.iter("Vertex"):
        xArray.append(x.get("X"))
    for y in tree.iter("Vertex"):
        yArray.append(y.get("Y"))
    for elem in range(len(xArray)):
        if count > 3:
            total = int(total / 4)
            xAvg.append(total)
            count = 0
            total = 0
        total += int(float(xArray[int(elem)]))
        count += 1
    count = 0
    total = 0
    for element in range(len(yArray)):
        if count > 3:
            total = int(total / 4)
            yAvg.append(total)
            count = 0
            total = 0
        total += int(float(yArray[int(element)]))
        count += 1
    name = os.path.splitext(filename)[0]
    nameTemp = name + "{}{}{}"
    img = cv2.imread(name + ".tif", 3)
    height, width = img.shape[:2]
    num = 0
    folder = r"C:\Users\Anna\Documents\GTC work\AdvAI\Individual Project\individual-project-master\original_images\negative images\\"
    coordinates = np.arange(width * height).reshape(width, height)
    size = width * height
    coordinates[-1, :] = 0
    coordinates[:, -1] = 0
    coordinates[0, :] = 0
    coordinates[:, 0] = 0
    coordinates[-1, :] = 0
    coordinates[:, -1] = 0
    coordinates[0, :] = 0
    coordinates[:, 0] = 0


    for i in range(len(xAvg)):
        point = xAvg[i] * yAvg[i]

        if len(np.where(coordinates == point)[0]) == 0 or len(np.where(coordinates == point)[1]) == 0: continue
        else:
            x_c = np.where(coordinates == point)[0][0]  #x index of coordinates(array) at a point
            y_c = np.where(coordinates == point)[1][0]   #y index of coordinates(array) at a point

        if coordinates[x_c,y_c] == 0: #if border, continue
           continue
        else:
            coordinates[x_c - 1:x_c + 2, y_c - 1:y_c + 2] = 0 #block out 3x3 section of positive areas
    newCoordinates = np.nonzero(coordinates)
    xC = []
    yC = []

    xC=newCoordinates[0]
    yC = newCoordinates[1]

    for k in range(0, len(xC), 4000):
        if coordinates[xC[k+1]][yC[k]] == 0 or coordinates[xC[k-1]][yC[k]] == 0 or coordinates[xC[k]][yC[k+1]] == 0 or coordinates[xC[k]][yC[k-1]] == 0: continue
        else:
            region = img[int(yC[k]-16):int(yC[k] + 16), int(xC[k]-16):int(xC[k] + 16)]
            height2, width2 = region.shape[:2]
            if height2 != 32 or width2 != 32: continue
            cv2.imwrite(os.path.join(folder, nameTemp.format("_", num, '.tif')), region)
            num += 1
    return 0

getLengthFolders()
negList, posList = getNegPosLists()
labelNeg, labelPos = makeLabels(negList, posList)
data, n_samples, total, totalLabel = reshape(labelPos, labelNeg, negList, posList)
total, totalLabel = randomizeArrays(total, totalLabel)

if __name__ == '__main__':
    main()
